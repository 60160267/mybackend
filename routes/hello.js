const express = require('express')
const router = express.Router()

/* GET users listing. */
router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({ message: 'Hello My name is Ford!!', params })
})

module.exports = router
